package aneo.kata.exceptions;

public class InvalidRectangleException extends Throwable {
    public InvalidRectangleException() {
        super("Invalid rectangle dimensions. Both dimensions must be positive integers.");
    }
}
