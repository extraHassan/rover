package aneo.kata.exceptions;

public class InvalidPositionException extends Exception {
    public InvalidPositionException() {
        super("Invalid position. Coordinates must be within the valid range.");
    }
}
