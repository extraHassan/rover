package aneo.kata;


import aneo.kata.enums.HeaderDirection;
import aneo.kata.exceptions.InvalidPositionException;
import aneo.kata.exceptions.InvalidRectangleException;
import aneo.kata.models.Rover;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
* @author Hassan EL MZABI
* @since 01/02/2024
* */
class Main {
    public static void main(String[] args) {
        final String space = " ";
        if (args.length != 1) {
            System.err.println("Usage: java -jar rover.jar input.txt");
            System.exit(1);
        }

        String inputFile = args[0];

        try (BufferedReader reader = new BufferedReader(new FileReader(inputFile))) {
            String firstLine = reader.readLine();
            if (firstLine == null) {
                throw new IOException("first line is null");
            }

            String[] firstLineParts = firstLine.split(space);
            int maxX = Integer.parseInt(firstLineParts[0]);
            int maxY = Integer.parseInt(firstLineParts[1]);

            String line;
            List<Rover> roverList = new ArrayList<>();

            while ((line = reader.readLine()) != null) {
                if (line.contains(space)) { // new aneo.kata.models.Rover
                    Rover rover = new Rover(maxX, maxY);
                    String[] parts = line.split(space);

                    rover.setPosition(
                            Integer.parseInt(parts[0]),
                            Integer.parseInt(parts[1]),
                            HeaderDirection.getInstanceByLetter(parts[2].charAt(0))
                    );
                    roverList.add(rover);
                } else { // give instruction for the last created rover
                    roverList.get(roverList.size() - 1).executeInstruction(line);
                }
            }

            // Output positions of rovers
            roverList.forEach(Rover::printPosition);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidRectangleException | InvalidPositionException e) {
            System.err.println("Error:" + e.getMessage());
            System.exit(1);
        }
    }
}
