package aneo.kata.validators;

public class RectangleValidator {
    public static boolean isValidRectangle(int maxX, int maxY) {
        // A valid rectangle must have positive dimensions
        return maxX > 0 && maxY > 0;
    }
}
