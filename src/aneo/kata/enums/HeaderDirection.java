package aneo.kata.enums;

public enum HeaderDirection {
    NORTH(1, 'N'),
    EAST(2, 'E'),
    SOUTH(3, 'S'),
    WEST(4, 'W');

    private final int value;
    private final char letter;

    HeaderDirection(int value, char letter) {
        this.value = value;
        this.letter = letter;
    }

    public static HeaderDirection getInstance(int value) throws IllegalArgumentException {
        switch (value) {
            case 1:
                return NORTH;
            case 2:
                return EAST;
            case 3:
                return SOUTH;
            case 4:
                return WEST;
            default:
                throw new IllegalArgumentException("Not a valid header direction value : " + value);
        }
    }

    public static HeaderDirection getInstanceByLetter(char letter) {
        switch (letter) {
            case 'N':
                return NORTH;
            case 'E':
                return EAST;
            case 'S':
                return SOUTH;
            case 'W':
                return WEST;
            default:
                throw new IllegalArgumentException("Not a valid header direction letter : " + letter);
        }
    }

    public char getLetter() {
        return letter;
    }

    public int getValue() {
        return value;
    }
}
