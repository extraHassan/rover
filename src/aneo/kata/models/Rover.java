package aneo.kata.models;

import aneo.kata.enums.HeaderDirection;
import aneo.kata.exceptions.InvalidPositionException;
import aneo.kata.exceptions.InvalidRectangleException;
import aneo.kata.validators.RectangleValidator;

/*
 * @author : Hassan EL MZABI
 */
public class Rover {
    private final int maxX;
    private final int maxY;
    private HeaderDirection headerDirection = HeaderDirection.NORTH;

    int x = 0;
    int y = 0;

    /**
     * Constructs a new aneo.kata.models.Rover object with the specified maximum x and y coordinates.
     *
     * @param maxX The maximum x coordinate.
     * @param maxY The maximum y coordinate.
     * @throws InvalidRectangleException If the provided rectangle dimensions are invalid.
     */
    public Rover(int maxX, int maxY) throws InvalidRectangleException {
        if (!RectangleValidator.isValidRectangle(maxX, maxY)) {
            throw new InvalidRectangleException();
        }
        this.maxX = maxX;
        this.maxY = maxY;
    }


    /**
     * Executes a sequence of instructions for the rover.
     *
     * @param instruction The instruction sequence.
     */
    public void executeInstruction(String instruction) {
        for (char order : instruction.toCharArray()) {
            conduct(order);
        }
    }


    /**
     * Sets the position of the rover.
     *
     * @param x               The x coordinate.
     * @param y               The y coordinate.
     * @param headerDirection The header direction where the rover is looking.
     * @throws InvalidPositionException If the provided position is invalid.
     */
    public void setPosition(int x, int y, HeaderDirection headerDirection) throws InvalidPositionException {
        if (x < 0 || x > maxX || y < 0 || y > maxY) {
            throw new InvalidPositionException();
        }
        this.x = x;
        this.y = y;
        this.headerDirection = headerDirection;
    }


    /**
     * Conducts a single instruction.
     *
     * @param order The instruction.
     */
    private void conduct(char order) {
        switch (order) {
            case 'L':
                turnLeft();
                break;
            case 'R':
                turnRight();
                break;
            case 'M':
                moveForward();
                break;
            default:
                throw new IllegalArgumentException("unrecognized conduct instruction : " + order);
        }
    }

    /**
     * Moves the rover forward based on its current heading direction.
     */
    private void moveForward() {
        switch (headerDirection) {
            case NORTH:
                if (this.y < maxY) {
                    this.y++;
                }
                break;
            case EAST:
                if (this.x < maxX) {
                    this.x++;
                }
                break;
            case SOUTH:
                if (this.y > 0) {
                    this.y--;
                }
                break;
            case WEST:
                if (this.x > 0) {
                    this.x--;
                }
                break;
        }
    }

    /**
     * Turns the rover to the right.
     */
    private void turnRight() {
        int newHeaderDirectionValue = this.headerDirection.getValue() + 1;
        if (newHeaderDirectionValue > 4) {
            this.headerDirection = HeaderDirection.NORTH;
        } else {
            this.headerDirection = HeaderDirection.getInstance(newHeaderDirectionValue);
        }
    }

    /**
     * Turns the rover to the left.
     */
    private void turnLeft() {
        int newHeaderDirectionValue = this.headerDirection.getValue() - 1;
        if (newHeaderDirectionValue < 1) {
            this.headerDirection = HeaderDirection.WEST;
        } else {
            this.headerDirection = HeaderDirection.getInstance(newHeaderDirectionValue);
        }
    }

    /**
     * Prints the current position of the rover.
     */
    public void printPosition() {
        System.out.println(this.x + " " + this.y + " " + this.headerDirection.getLetter());
    }


}




