# Rover Navigation Program

## Introduction
This program simulates the navigation of rovers on a rectangular grid. Each rover is given initial coordinates and orientation, and a series of instructions to move and turn.

## How to Run
1. **Ensure Java is Installed**: Make sure you have Java Development Kit (JDK) version 8 or higher installed on your system.
   
2. **Download the rover.jar File**: Download the provided rover.jar file.

3. **Prepare Input File**: Prepare an input.txt file containing the following information:
   - First line: Dimensions of the rectangular grid (e.g., "5 5" for a 5x5 grid).
   - Subsequent lines: Each line describes a rover, its initial position (x y) and orientation (N, E, S, W), followed by a series of instructions for the rover (e.g., "1 2 N", "LMLMLMLMM").

4. **Run the Program**: Open a command prompt or terminal window and navigate to the directory containing the rover.jar file and the input.txt file.

5. **Execute the Program**: Run the following command: java -jar rover.jar input.txt

Replace `input.txt` with the name of your input file if it's different.

6. **View Output**: The program will display the final positions of all rovers after executing the instructions.

## Example Input
5 5  
1 2 N  
LMLMLMLMM  
3 3 E  
MMRMMRMRRM  
  
## Output
1 3 N  
5 1 E


## Strategy Explanation
The program employs a straightforward object-oriented approach to simulate rover navigation on a rectangular grid. Each rover is represented by an instance of the Rover class, which encapsulates its current position (x, y) and orientation (N, E, S, W). 

Instructions for moving and turning the rover are processed sequentially, with each instruction triggering a corresponding method call (`executeInstruction`) on the rover object.

The `executeInstruction` method interprets the instructions provided and delegates the task of moving, turning, or reporting the rover's position to other private methods within the Rover class (`moveForward`, `turnLeft`, `turnRight`). 

Error handling for invalid positions and rectangles is implemented using custom exception classes (`InvalidPositionException`, `InvalidRectangleException`). Overall, the code is structured, modular, and easy to understand, facilitating efficient navigation of multiple rovers on the grid.

